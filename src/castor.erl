-module(castor).

-include("header.hrl").

-include_lib("conveyor/include/header.hrl").

-export([
    test_load/1,
    test_send/1,
    test/0
]).

%%====================================================================
%% API
%%====================================================================

test_load(Count) ->
    conveyor:start(#pool{
        name = randomize,
        belts = 16,
        exec = fun(_State, Min, Max) -> castor_util:random(Min, Max) end,
        pipe_to = generate,
        pool_strategy = fifo
    }),
    conveyor:start(#pool{
        name = generate,
        max_belts = 16,
        exec = fun(_State, Size) -> {Size, list_to_binary(castor_util:random_string(Size))} end,
        pipe_to = store
    }),
    conveyor:start(#pool{
        name = store,
        max_belts = 16,
        exec = fun(_State, {Size, Body}) ->
            castor_db:equery([
                "INSERT INTO emails",
                "(queue, size, from_field, to_field, subject, body)",
                "VALUES ($1, $2, $3, $4, $5, $6)"
            ],[
                test,
                Size,
                "user1@example.com",
                "blackhole@localhost",
                "Test message",
                Body
            ])
        end
    }),
    [conveyor:async_enqueue(randomize, [X * 1024, 1024 * 1024]) || X <- lists:seq(1, Count)].

test_send(QueueName) ->
    conveyor:start(#pool{
        name = grab,
        belts = 3,
        produce_interval = 1000,
        exec = fun(_State) ->
            {ok, _Colls, Data} = castor_db:equery("SELECT * FROM emails_pack($1, $2, 1)", [QueueName, 1048576]),
            Data
        end,
        pipe_to = accamulate
    }),
    conveyor:start(#pool{
        name = accamulate,
        belts = 3,
        exec = fun(_State, Emails) ->
            lists:map(fun({_, _, _, From, To, Subject, Body, _, _, _}) ->
                {From, To, Subject, Body}
            end, Emails)
        end,
        pipe_to = send,
        pipe_type = async,
        pipe_each = true
    }),
    conveyor:start(#pool{
        name = send,
        belts = 3,
        pool_strategy = fifo,
        worker_data = [
            {relay, "postfix.firefly.red"},
            {port, 9267},
            {auth, never},
            {username, "user1"},
            {password, "12345"}
        ],
        init = fun(#pool{worker_data = Data} = Pool) ->
            smtpc_session:start(Data)
        end,
        exec = fun({ok, SessionId}, {From, To, Subject, Body}) ->
            Data = smtpc_util:mail_body_form(From, To, Subject, Body),
            smtpc_session:send_sync(SessionId, From, To, Data)
        end,
        term = fun(_Pool, {ok, SessionId}) ->
            smtpc_session:stop(SessionId)
        end
    }).

test() ->
    test_send(test).

%%====================================================================
%% Internal functions
%%====================================================================
