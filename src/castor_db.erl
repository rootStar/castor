%%% @author   Nick Roshin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roshin.
%%% @doc

-module(castor_db).

-behaviour(gen_server).

-include("header.hrl").

-define(SERVER, ?MODULE).

-author('Nick Roshin <deneb808@gmail.com>').

%% ===================================================================
%% API Function Exports
%% ===================================================================

-export([
    start_link/0,
    equery/2,
    equery/3,
    squery/1,
    squery/2
]).

%% ===================================================================
%% gen_server Function Exports
%% ===================================================================

-export([init/1,
    handle_call/3,
    handle_cast/2,
    terminate/2,
    handle_info/2,
    code_change/3]).

-record(state, {conn}).

%% ===================================================================
%% API Function Definitions
%% ===================================================================

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

-spec equery(Sql::epgsql:sql_query(), Params :: list(epgsql:bind_param())) -> epgsql:reply(epgsql:equery_row()).
equery(Sql, Params) ->
    equery(epgsql_pool, Sql, Params).

-spec equery(PoolName::atom(), Sql::epgsql:sql_query(), Params :: list(epgsql:bind_param())) -> epgsql:reply(epgsql:equery_row()).
equery(PoolName, Sql, Params) ->
    poolboy:transaction(PoolName, fun(Worker) ->
        gen_server:call(Worker, {equery, Sql, Params})
    end).

-spec squery(Sql::epgsql:sql_query()) -> epgsql:reply(epgsql:squery_row()) | [epgsql:reply(epgsql:squery_row())].
squery(Sql) ->
    squery(epgsql_pool, Sql).

-spec squery(PoolName::atom(), Sql::epgsql:sql_query()) -> epgsql:reply(epgsql:squery_row()) | [epgsql:reply(epgsql:squery_row())].
squery(PoolName, Sql) ->
    poolboy:transaction(PoolName, fun(Worker) ->
        gen_server:call(Worker, {squery, Sql})
    end).

%% ===================================================================
%% gen_server Function Definitions
%% ===================================================================

init([]) ->
    {ok, Boostrap} = file:read_file([code:priv_dir(castor), "/bootstrap.sql"]),
    case squery(Boostrap) of
        {error, Error} ->
            error_logger:warning_msg("Boostrap failed ~p~n", [Error]);
        Data ->
            error_logger:info_msg("Boostrap succeed ~p~n", [Data])
    end,
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, #state{}) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ===================================================================
%% Internal Function Definitions
%% ===================================================================
