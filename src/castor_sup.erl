%%%-------------------------------------------------------------------
%% @doc castor top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(castor_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->    
    Pools = application:get_env(castor, pools, []),
    PoolSpec = lists:map(fun ({PoolName, SizeArgs, WorkerArgs}) ->
                            Args = case proplists:get_value(name, SizeArgs) of
                                undefined ->
                                    [{name, {local, PoolName}} | SizeArgs];
                                _Value ->
                                    SizeArgs
                            end,
                            poolboy:child_spec(PoolName, Args, WorkerArgs)
                         end, Pools),

    {ok, {{one_for_one, 1, 5}, PoolSpec ++ [
            {
                castor_db,
                {castor_db, start_link, []},
                permanent,
                5000,
                worker,
                [castor_db]
            }
        ]}
    }.

%%====================================================================
%% Internal functions
%%====================================================================
