-module(castor_util).

-include("header.hrl").

-export([
    random/2,
    random_string/1,
    get_mail_size/3
]).

random_string(Len) ->
    Chrs = list_to_tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),
    ChrsSize = size(Chrs),
    F = fun(_, R) -> [element(random:uniform(ChrsSize), Chrs) | R] end,
    lists:foldl(F, "", lists:seq(1, Len)).

random(Min, Max) ->
    random:seed(erlang:monotonic_time()),
    round(random:uniform() * (Max - Min) + Min).

get_mail_size(From, [First | _Tail] = To, Body) when is_list(To) andalso is_list(First) ->
    iolist_size([
        "MAIL FROM: <", From, ">\r\n",
        "RCPT TO: ",To,"\r\n",
        "DATA\r\n",
        Body,
        "\r\n.\r\n"
    ]) + length(To) * 2;
get_mail_size(From, To, Body) ->
    get_mail_size(From, [To], Body).
