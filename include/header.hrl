
-record(queues, {id, max_workers, bandwidth_limit, credentials}).

-record(emails, {queue, size, from_field, to_field, body}).


-define(TRAFFIC_MAX, 1024).

-define(SENDING_TIMEOUT, 1000).

-define(GETTING_TIMEOUT, 0).

-define(MIN_MAIL_SIZE, 300).

-define(MAX_MAIL_SIZE, 1024 * 1024).
