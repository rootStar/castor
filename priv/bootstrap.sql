CREATE TABLE emails
(
    id serial NOT NULL,
    queue character varying(255) NOT NULL,
    size integer NOT NULL,
    from_field character varying(255) NOT NULL,
    to_field text NOT NULL,
    subject character varying(255) NOT NULL,
    body text NOT NULL,
    status integer DEFAULT 1,
    created_at integer,
    updated_at integer,
    CONSTRAINT emails_pkey PRIMARY KEY (id)
);

CREATE TABLE queues
(
  id serial NOT NULL,
  name character varying(255) NOT NULL,
  connections integer NOT NULL,
  bandwidth_limit integer NOT NULL,
  credentials text,
  created_at integer,
  updated_at integer,
  CONSTRAINT queues_pkey PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION emails_pack(queue_ varchar(255), total_size integer, status_ integer = 1) RETURNS SETOF emails AS
$BODY$
DECLARE
	sizes integer = 0;
	query emails%rowtype;
BEGIN
    LOCK TABLE emails IN ACCESS EXCLUSIVE MODE;
	LOOP
		SELECT * FROM emails WHERE queue = queue_ AND status = status_ AND size < (total_size - sizes) LIMIT 1 INTO query;
		IF query IS NULL THEN
			EXIT;
		ELSE
            UPDATE emails SET status = 2 WHERE id = query.id;
            sizes := sizes + query.size;
            RETURN NEXT query;
		END IF;
	END LOOP;
END;
$BODY$
LANGUAGE plpgsql;
